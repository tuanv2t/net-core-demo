﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostgreDemo
{
    public class Account//Unable to use name User for table on Postgres
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
