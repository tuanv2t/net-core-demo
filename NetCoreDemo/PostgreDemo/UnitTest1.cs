using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostgreDemo
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            using (var db = new PerformanceTestContext())
            {
            }
        }

        [TestMethod]
        public void CreateUser_OK()
        {
            using (var db = new PerformanceTestContext())
            {
                var item = new Account
                {
                    Id = Guid.NewGuid().ToString(),
                    UserName = "AAAAA",
                    FirstName = "AAA",
                    LastName = "AAAAL"
                };
                db.Accounts.Add(item);
                db.SaveChanges();
            }
        }
        public string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }
        [TestMethod]
        public void CreateManyUser_OK()
        {
            //Generate one milion user 
            var numberOfItems = 1000000;
            //var userList = new List<Account>(numberOfItems);
            var existingData = new Dictionary<string, string>(1);
            using (var db = new PerformanceTestContext())
            {
                
                var userNameList = db.Accounts.Select(x => x.UserName).ToList();//Sometime the UserName is random duplicate with existing in database ??
                existingData = new Dictionary<string, string>(userNameList.Count);
                foreach(var item in userNameList)
                {
                    existingData[item] = string.Empty;
                }
            }

            var dic = new Dictionary<string, Account>(numberOfItems);
           
            

                int count = 0;
            Account newAcc = null;
            while (count < numberOfItems)
            {
                newAcc = new Account()
                {
                    Id = Guid.NewGuid().ToString(),
                    UserName = RandomString(50, false),//Sometime the UserName is random duplicate with existing in database ??
                    FirstName = RandomString(50, false),
                    LastName = RandomString(50, false)
                };
                if (!dic.ContainsKey(newAcc.UserName) && !existingData.ContainsKey(newAcc.UserName))
                {
                    dic[newAcc.UserName] = newAcc;
                    count++;
                }
                //if (!userList.Exists(x => x.UserName.Equals(newAcc.UserName)))
                //    {
                //    userList.Add(newAcc);
                //    count++;
                //}
            }
           
            int batchInsert = 0;
            using (var db = new PerformanceTestContext())
            {
                if (db.Accounts.Count() > 2000000)
                {
                    return;
                }
                //foreach (var user in userList)
                foreach (KeyValuePair<string, Account> entry in dic)
                {
                    batchInsert++;
                    db.Accounts.Add(entry.Value);
                    if (batchInsert == 10000)
                    {
                        db.SaveChanges();
                        batchInsert = 0;
                    }


                }
                db.SaveChanges();//all remaining items
            }




        }

        [TestMethod]
        public void GetUser_OK()
        {
            //Note: you have to create index manually CREATE INDEX UserName_Idx on Users(UserName)
            using (var db = new PerformanceTestContext())
            {
                var start = DateTime.Now;
                var user = db.Accounts.FirstOrDefault(x => x.UserName.Equals("AAAAA"));
                var end = DateTime.Now;
                var duration = end.Subtract(start).TotalMilliseconds;
                //System.Diagnostics.Debug.WriteLine(duration);
                System.IO.File.CreateText(duration.ToString() + ".txt");
            }
        }

    }
}
