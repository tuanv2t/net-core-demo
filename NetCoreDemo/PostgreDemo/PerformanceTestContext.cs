﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Options;
using Npgsql;
using Npgsql.NameTranslation;

namespace PostgreDemo
{
    public class PerformanceTestContext : DbContext
    {
        public DbSet<Account> Accounts { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //Note: A mysql must be created manually before with the table User as below
            var connectionString = "User ID =postgres;Password=sa123;Server=localhost;Port=5432 ;Database=performanceTest";
            optionsBuilder.UseNpgsql(connectionString);
                //.UseSnakeCaseNamingConvention(); //Apply a library : https://github.com/efcore/EFCore.NamingConventions/  to ignore bug lower-case. Postgres uses only lower case name for tables, columns,... => still error, postgress will convert FirstName to first_name to look in database 
            //Note a problem with Postgres : https://github.com/npgsql/efcore.pg/issues/21 => efcore3: https://github.com/dotnet/efcore/issues/5159#issuecomment-522223918
            // Apply a library : https://github.com/efcore/EFCore.NamingConventions/
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
			base.OnModelCreating(modelBuilder);
			modelBuilder.Entity<Account>()
              .ToTable("account");
			//modelBuilder.UseSnakeCaseNames();//Still error with FirstName => first_name
			modelBuilder.Entity<Account>(e => e.Property(p => p.Id).HasColumnName("id"));
			modelBuilder.Entity<Account>(e => e.Property(p => p.FirstName).HasColumnName("firstname"));
			modelBuilder.Entity<Account>(e => e.Property(p => p.LastName).HasColumnName("lastname"));
			modelBuilder.Entity<Account>(e => e.Property(p => p.UserName).HasColumnName("username"));
		}
        /*Run this script to create table Account */
        /*
         
            CREATE TABLE Account(
               Id VARCHAR(50) PRIMARY KEY,
               UserName VARCHAR (50) UNIQUE NOT NULL,
               FirstName VARCHAR (50) NOT NULL,
               LastName VARCHAR (355)  NOT NULL
 
            );
         */
    }
	public static class StringExtensions
	{
		public static string ToSnakeCase(this string input)
		{
			if (string.IsNullOrEmpty(input)) { return input; }

			var startUnderscores = Regex.Match(input, @"^_+");
			return startUnderscores + Regex.Replace(input, @"([a-z0-9])([A-Z])", "$1_$2").ToLower();
		}
	}
	public static class ModelBuilderExtensions
	{
		static readonly Regex _keysRegex = new Regex("^(PK|FK|IX)_", RegexOptions.Compiled);

		public static void UseSnakeCaseNames(this ModelBuilder modelBuilder)
		{
			var mapper = new NpgsqlSnakeCaseNameTranslator();

			foreach (var table in modelBuilder.Model.GetEntityTypes())
			{

				ConvertToSnake(mapper, table);

				foreach (var property in table.GetProperties())
				{
					ConvertToSnake(mapper, property);
				}

				foreach (var primaryKey in table.GetKeys())
				{
					ConvertToSnake(mapper, primaryKey);
				}

				foreach (var foreignKey in table.GetForeignKeys())
				{
					ConvertToSnake(mapper, foreignKey);
				}

				foreach (var indexKey in table.GetIndexes())
				{
					ConvertToSnake(mapper, indexKey);
				}
			}
		}

		static void ConvertToSnake(INpgsqlNameTranslator mapper, object entity)
		{
			switch (entity)
			{
				case IMutableEntityType table:
					table.SetTableName(ConvertGeneralToSnake(mapper, table.GetTableName()));
					break;
				case IMutableProperty property:
					property.SetColumnName(ConvertGeneralToSnake(mapper, property.GetColumnName()));
					break;
				case IMutableKey primaryKey:
					primaryKey.SetName(ConvertKeyToSnake(mapper, primaryKey.GetName()));
					break;
				case IMutableForeignKey foreignKey:
					foreignKey.SetConstraintName(ConvertKeyToSnake(mapper, foreignKey.GetConstraintName()));
					break;
				case IMutableIndex indexKey:
					indexKey.SetName(ConvertKeyToSnake(mapper, indexKey.GetName()));
					break;
				default:
					throw new NotImplementedException("Unexpected type was provided to snake case converter");
			}
		}

		static string ConvertKeyToSnake(INpgsqlNameTranslator mapper, string keyName) =>
			ConvertGeneralToSnake(mapper, _keysRegex.Replace(keyName, match => match.Value.ToLower()));

		static string ConvertGeneralToSnake(INpgsqlNameTranslator mapper, string entityName) =>
			mapper.TranslateMemberName(entityName);
	}
}
