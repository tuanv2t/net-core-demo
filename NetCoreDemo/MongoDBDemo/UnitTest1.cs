using Microsoft.VisualStudio.TestTools.UnitTesting;
using  MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MongoDBDemo
{
    [TestClass]
    public class UnitTest1
    {
        private string databaseName = "performanceTest";
        private string connectionString = "mongodb://localhost:27017/performanceTest";//no authorization

        private IMongoDatabase GetDatabases() {
            var client = new MongoClient(connectionString);
            var database = client.GetDatabase(databaseName);
            return database;
        }
        [TestMethod]
        public void ConnectDatabase_OK()
        {
            //Connect to a mongodb server 
           
            var database = GetDatabases();
            Assert.IsNotNull(database);
        }
        [TestMethod]
        public void GetCollection_OK()
        {
            var db  = GetDatabases();
            var users= db.GetCollection<User>("User");//You can manually create a collection User before

            Assert.IsNotNull(users);
        }
        [TestMethod]
        public void CreateUser_OK()
        {
            var db = GetDatabases();
            var users = db.GetCollection<User>("User");//You can manually create a collection User before
            var item = new User
            {
                UserName = "AAAAA",
                FirstName = "AAA",
                LastName = "AAAAL" };
            users.InsertOne(item);
            Assert.IsNotNull(item.Id);
        }

        public string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }
        [TestMethod]
        public void CreateManyUser_OK()
        {
            var db = GetDatabases();
            var users = db.GetCollection<User>("User");//You can manually create a collection User before
            var count = users.CountDocuments(x=>x.UserName!=null);
            if(count > 1000000)
            {
                return;
            }
            //Generate one milion user 
            var numberOfItems = 1000000;
            var userList = new List<User>(numberOfItems);
            for(var i = 0; i < numberOfItems; i++)
            {
                userList.Add(new User()
                {
                    UserName = RandomString(50, false),
                    FirstName = RandomString(50,false),
                    LastName = RandomString(50,false)
                }) ;
            }
            foreach(var user in userList)
            {
                users.InsertOne(user);
            }
            
         
        }
        [TestMethod]
        public async Task GetUser_OK()
        {
            var db = GetDatabases();
            var users = db.GetCollection<User>("User");
            var start = DateTime.Now;
            var user = await users.Find(x => x.UserName.Equals("AAAAA")).ToListAsync();
            var end = DateTime.Now;
            var duration = end.Subtract(start).TotalMilliseconds;
            //System.Diagnostics.Debug.WriteLine(duration);
            System.IO.File.CreateText(duration.ToString() + ".txt");


        }
    }
}
