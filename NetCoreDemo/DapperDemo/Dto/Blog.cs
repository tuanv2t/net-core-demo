﻿using System;
using System.Collections.Generic;
using System.Text;
using DapperDemo.DtoAttributes;
namespace DapperDemo.Dto
{
    public class Blog
    {
        [DtoKey]
        public int BlogId { get; set; } //BlogId need to be marked as a key 
        public string Url { get; set; }
    }
}
