﻿using System;
using DapperDemo.Dto;
using Dapper;
using System.Data.SqlClient;//included along with Dapper
using System.Collections;//included along with Dapper
using System.Collections.Generic;//included along with Dapper
using System.Threading.Tasks;
namespace DapperDemo
{
    public class BlogRepository: DapperRepositoryBase
    {
        //private string _connectionString;
        public BlogRepository(string connectionString):base(connectionString)
        {
            //this._connectionString = connectionString;
        }
        public IEnumerable<Blog> GetAll()
        {
            using (var conn = new SqlConnection(this.ConnectionString))
            {
                //Execute a query and map the results to a strongly typed List
                var data = conn.Query<Blog>("SELECT * FROM Blog");//using directly SqlMapper of Dapper
                return data;
            }
        }
        public Blog Insert(Blog newBlog)//use DapperRepositoryBase
        {
            using (var conn = new SqlConnection(this.ConnectionString))
            {
                this.Insert<Blog>(newBlog);
                if (newBlog.BlogId <= 0)//BlogID will be automatically gotten from database after inserting 
                {
                    throw new Exception("Unable to insert blog");
                }
                return newBlog;
            }
        }
        public async Task<IEnumerable<Blog>> GetAllAsync()
        {
            using (var conn = new SqlConnection(this.ConnectionString))
            {
                //Execute a query and map the results to a strongly typed List
                var query = conn.QueryAsync<Blog>("SELECT * FROM Blog");//using directly SqlMapper of Dapper
                return await query;
            }
        }
    }
}
