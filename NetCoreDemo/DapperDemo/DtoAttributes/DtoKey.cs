﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DapperDemo.DtoAttributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class DtoKey : Attribute
    {
    }
}
