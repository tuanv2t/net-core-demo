﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IdentityServer4ClientDemo.Models;
using IdentityModel.Client;
using Microsoft.AspNetCore.Http;
using System.Net.Http;

namespace IdentityServer4ClientDemo.Controllers
{
    public class HomeController : Controller
    {
        public async Task<IActionResult> Index()
        {
            // discover endpoints from metadata
            var disco = await DiscoveryClient.GetAsync("http://localhost:5000");
            ViewBag.Error = string.Empty;
            if (disco.IsError)
            {
                ViewBag.Error = disco.Error;
                return View();
            }
            // request token
            var tokenClient = new TokenClient(disco.TokenEndpoint, "client", "secret");
            var tokenResponse = await tokenClient.RequestClientCredentialsAsync("api1");

            if (tokenResponse.IsError)
            {
                ViewBag.Error =tokenResponse.Error;
                return View();
            }

            ViewBag.Error = tokenResponse.Json;
            // call api
            var client = new HttpClient();
            client.SetBearerToken(tokenResponse.AccessToken);

            var response = await client.GetAsync("http://localhost:5001/identity");
            if (!response.IsSuccessStatusCode)
            {
                ViewBag.Error = response.StatusCode;
            }
            else
            {
                var content = await response.Content.ReadAsStringAsync();
                ViewBag.Error = content;
            }
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
