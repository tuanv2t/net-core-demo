using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HtmlAgilityPack;
using System.Linq;// included along with HtmlAgilityPack.NetCore
namespace HtmlAgilityPackDemo.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ParseSimpleHtml()
        {
            //Try to use HtmlAgilityPack.NetCore to parse html (https://www.nuget.org/packages/HtmlAgilityPack.NetCore/)
            var html = System.IO.File.ReadAllText(@"Sample Data\HtmlAgilityPack.html");
            HtmlDocument doc = new HtmlDocument();//using HtmlAgilityPack
            doc.LoadHtml(html);
            Assert.IsNotNull(doc);
            //get body
            var body = doc.DocumentNode.Descendants().Where(node => node.Name == "body");
            Assert.IsNotNull(body);

        }
    }
}
