﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DapperDemo;
using DapperDemo.Dto;
namespace DapperDemo.WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class BlogController : Controller
    {
        // GET api/blog
        [HttpGet]
        public IEnumerable<Blog> Get()
        {
            var repo = new BlogRepository("Data Source=DESKTOP-0A0HNT0\\SQLEXPRESS;Initial Catalog=Blogging;User ID=sa;Password=sa@123");
            var blogs = repo.GetAll().ToList();
            return blogs;
        }

        
    }
}
