﻿using System;
using Microsoft.Extensions.Configuration;//Nuget Microsoft.Extensions.Configuration
using System.IO;

namespace AppsettingsDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory()) //Nuget Microsoft.Extensions.Configuration.FileExtensions to use SetBasePath
                   .AddJsonFile("appsettings.json");// Nuget Microsoft.Extensions.Configuration.Json to use AddJsonFile
            var config = builder.Build();

            var appConfig = new AppOptions();
            config.GetSection("App").Bind(appConfig);//Nuget Microsoft.Extensions.Configuration.Binder to use .Bind
                                                     /*
                                                      * ASP.NET Core 1.1 and higher can use Get<T>, which works with entire sections. 
                                                      * Get<T> can be more convienent than using Bind. 
                                                      * The following code shows how to use Get<T> with the sample above:
                                                      */
                                                     //var appConfig = config.GetSection("App").Get<AppOptions>();

            Console.WriteLine(appConfig.Connection.Value);
            Console.WriteLine(appConfig.Profile.Machine);
            Console.WriteLine(appConfig.Window.Height);
        }
    }
}