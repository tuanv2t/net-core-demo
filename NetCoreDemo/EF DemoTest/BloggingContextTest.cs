using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EF_Demo.Models;

namespace EF_DemoTest
{
    [TestClass]
    public class BloggingContextTest
    {
        [TestMethod]
        public void AddBlog_OK()
        {
            using (var db = new BloggingContext())
            {
                var blog = new Blog { Url = "http://sample.com" };
                db.Blog.Add(blog);
                db.SaveChanges();
            }

            //using (var context = new BloggingContext())
            //{
            //    var blogs = context.Blog.ToList();
            //}
        }

        //[TestMethod]
        //public void LoadAllBlog_OK()
        //{

        //    using (var context = new BloggingContext())
        //    {
        //        var blogs = context.Blog.ToList();//no find ToList
        //    }
        //}
    }
}
