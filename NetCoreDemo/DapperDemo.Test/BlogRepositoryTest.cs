using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;//included along with Dapper
using DapperDemo.Dto;
using System.Threading.Tasks;

namespace DapperDemo.Test
{
    [TestClass]
    public class BlogRepositoryTest
    {
        private string connectionString = "Data Source=DESKTOP-0A0HNT0\\SQLEXPRESS;Initial Catalog=Blogging;User ID=sa;Password=sa@123";
        [TestMethod]
        public void GetAll_OK()
        {
            var repo = new BlogRepository(connectionString);
            var blogs = repo.GetAll().ToList();
            Assert.IsNotNull(blogs);
            Assert.AreEqual(blogs[0].BlogId, 1);
        }

        [TestMethod]
        public void Insert_OK()
        {
            var repo = new BlogRepository(connectionString);
            var newBlog = new Blog();
            newBlog.Url = DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss");
            var url = newBlog.Url;
            repo.Insert(newBlog);
           
            var blogs = repo.GetAll().ToList();
            var alreadyInsertedBlog = blogs.Where(x => x.Url.Equals(url)).First();
            Assert.IsNotNull(alreadyInsertedBlog);
            Assert.AreEqual(alreadyInsertedBlog.BlogId, newBlog.BlogId);//Check to make sure that the BlogID has been gotten to set to newBlog
        }

        [TestMethod]
        public async Task GetAllAsync_OK()
        {
            try
            {
                var repo = new BlogRepository(connectionString);
                var query = await repo.GetAllAsync();
                var blogs = query.ToList();
                Assert.IsNotNull(blogs);
                Assert.AreEqual(blogs[0].BlogId, 1);
            }
            catch(Exception ex)
            {
                throw ex;
            }
          
        }
    }
}
