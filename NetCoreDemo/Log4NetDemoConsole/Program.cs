﻿using System;
using log4net;
using log4net.Config;
using log4net.Repository;
using System.IO;
namespace Log4NetDemoConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            ILoggerRepository repository = LogManager.CreateRepository("Log4NetDemoConsole");
            XmlConfigurator.Configure(repository, new FileInfo("log4net.xml"));
            ILog log = LogManager.GetLogger(repository.Name, "Log4NetDemoConsole.Root");

            log.Info("Hello log");
            log.Error("error");
            log.Warn("warn");

            //If you want to reuse the repository Log4NetDemoConsole again, => Not able to create, jsut get 
            ILog log2 = LogManager.GetLogger("Log4NetDemoConsole", "Log4NetDemoConsole.Root");
            log2.Info("Hello log2");

        }
    }
}
