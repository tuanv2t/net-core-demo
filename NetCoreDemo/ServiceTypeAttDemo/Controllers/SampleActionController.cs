﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ServiceTypeAttDemo.Securities;

namespace ServiceTypeAttDemo.Controllers
{
    public class SampleActionController : Controller
    {
        [SampleActionFilter]
        public IActionResult Index()
        {
            return Content("Examine the headers using developer tools.");
        }

       
    }
}