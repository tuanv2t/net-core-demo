﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ServiceTypeAttDemo.Models;
using ServiceTypeAttDemo.Securities;
namespace ServiceTypeAttDemo.Controllers
{
    [AddHeader("Author", "Steve Smith @ardalis")]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return Content("Examine the headers using developer tools.");
            //return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }
        public IActionResult SomeResource()
        {
            return Content("Successful access to resource - header should be set.");
        }
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [TypeFilter(typeof(AddHeaderAttribute),
            Arguments = new object[] { "Author", "Steve Smith (@ardalis)" })]
        public IActionResult Hi(string name)
        {
            return Content($"Hi {name}");
        }
    }
}
