﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Log4NetDemo.Models;
using Microsoft.Extensions.Logging;

namespace Log4NetDemo.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger _logger;
        public HomeController(ILoggerFactory DepLoggerFactory)
        {
            _logger  = DepLoggerFactory.CreateLogger("HomeController"); ;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            _logger.LogError("LogError");
            _logger.LogCritical("LogCritical");
            _logger.LogDebug("LogDebug");
            _logger.LogWarning("LogWarning");
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
