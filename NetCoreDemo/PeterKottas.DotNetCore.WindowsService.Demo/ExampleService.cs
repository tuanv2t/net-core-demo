﻿using System;
using System.Collections.Generic;
using System.Text;
using PeterKottas.DotNetCore.WindowsService.Base;
using PeterKottas.DotNetCore.WindowsService.Interfaces;


namespace PeterKottas.DotNetCore.WindowsService.Demo
{
    public class ExampleService: MicroService,IMicroService
    {
        public void Start()
        {
            this.StartBase();
            Timers.Start("Poller", 1000, () =>
            {
                Console.WriteLine("Polling at {0}\n", DateTime.Now.ToString("o"));
            },
            (e) =>
            {
                Console.WriteLine("Exception while polling: {0}\n", e.ToString());
            });
            Console.WriteLine("I started");
        }

        public void Stop()
        {
            this.StopBase();
            Console.WriteLine("I stopped");
        }
    }
}
