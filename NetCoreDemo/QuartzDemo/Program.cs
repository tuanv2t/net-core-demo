﻿using System;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;
using System.Threading;

namespace QuartzDemo
{
    class Program
    {
        public static void Main(string[] args)
        {
            TestDemoJob1().GetAwaiter().GetResult();
            Console.ReadKey();
        }

        public static async Task TestDemoJob1()
        {
            IScheduler scheduler;
            var schedulerFactory = new StdSchedulerFactory();
            scheduler = schedulerFactory.GetScheduler().Result;
            scheduler.Start().Wait();

            int ScheduleIntervalInMinute = 1;//job will run every minute 
            JobKey jobKey = JobKey.Create("DemoJob1");

            IJobDetail job = JobBuilder.Create<DemoJob1>().WithIdentity(jobKey).Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("JobTrigger")
                .StartNow()
                .WithSimpleSchedule(x => x.WithIntervalInMinutes(ScheduleIntervalInMinute).RepeatForever())
                .Build();

            await scheduler.ScheduleJob(job, trigger);
        }
    }
}