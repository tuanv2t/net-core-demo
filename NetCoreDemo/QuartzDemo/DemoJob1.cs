﻿using System;
using System.Collections.Generic;
using System.Text;
using Quartz;
using System.Threading.Tasks;// included along with Quartz 
namespace QuartzDemo
{
    public class DemoJob1:IJob
    {
        public DemoJob1()
        {

        }
        public Task Execute(IJobExecutionContext context)
        {
            //simple task, the job just prints current datetime in console
            //return Task.Run(() => {
            //    //Console.WriteLine(DateTime.Now.ToString());
            //});
            Console.WriteLine(DateTime.Now.ToString());
            return Task.FromResult(0);
        }
    }
}
