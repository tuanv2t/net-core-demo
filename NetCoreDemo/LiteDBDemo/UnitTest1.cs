﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LiteDB;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace LiteDBDemo
{
    [TestClass]
    public class UnitTest1
    {

        string litedbPath = AppDomain.CurrentDomain.BaseDirectory + "//LiteDBTest.db";
        [TestMethod]
        public void CreateDB_Many_Collections_OK()
        {
            using (var db = new LiteDatabase(litedbPath))

            {
                var col1 = db.GetCollection <Collection1>
                   ("Collection1");

            }

            using (var db = new LiteDatabase(litedbPath))

            {
                var col2 = db.GetCollection<Collection2>
                   ("Collection2");

            }
        }
        public class Collection1
        {
            public string Id { get; set; }
        }
        public class Collection2
        {
            public string Id { get; set; }
        }
    }


}
