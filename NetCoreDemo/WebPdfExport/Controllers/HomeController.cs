﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebPdfExport.Models;
using System.IO;
using WkWrap.Core;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebPdfExport.Controllers
{
    public class HomeController : Controller
    {
        private ICompositeViewEngine _viewEngine;
        public HomeController(ICompositeViewEngine viewEngine) {
            _viewEngine = viewEngine;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult DownloadSimpleTextPdf()
        {
            var htmlContent = "<div>Hello World</div>";
            //var wkhtmltopdf = new FileInfo(@"C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe");
            var wkhtmltopdf = new FileInfo(Directory.GetCurrentDirectory() + @"\PdfEngine\wkhtmltopdf.exe");
            var converter = new HtmlToPdfConverter(wkhtmltopdf);
            var pdfBytes = converter.ConvertToPdf(htmlContent);

            FileResult fileResult = new FileContentResult(pdfBytes, "application/pdf");
            fileResult.FileDownloadName = "test.pdf";
            return fileResult;
        }
        private async Task<string> RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.ActionDescriptor.ActionName;

            ViewData.Model = model;

            using (var writer = new StringWriter())
            {
                ViewEngineResult viewResult =
                    _viewEngine.FindView(ControllerContext, viewName, false);

                ViewContext viewContext = new ViewContext(
                    ControllerContext,
                    viewResult.View,
                    ViewData,
                    TempData,
                    writer,
                    new HtmlHelperOptions()
                );

                await viewResult.View.RenderAsync(viewContext);

                return writer.GetStringBuilder().ToString();
            }
        }
        [HttpPost]
        public async Task<IActionResult> DownloadEmployeePdf()
        {
            var employeeList = new List<Employee>();
            employeeList.Add(new Employee() {
            FirstName="John",
            LastName="Henrry"});

            employeeList.Add(new Employee()
            {
                FirstName = "Donal",
                LastName = "Trump"
            });
            var renderedView = await RenderPartialViewToString("EmployeeExportPdf", employeeList);
            var wkhtmltopdf = new FileInfo(Directory.GetCurrentDirectory() + @"\PdfEngine\wkhtmltopdf.exe");
            var converter = new HtmlToPdfConverter(wkhtmltopdf);
            var pdfBytes = converter.ConvertToPdf(renderedView);

            FileResult fileResult = new FileContentResult(pdfBytes, "application/pdf");
            var pdfFileName = "Employees_Html.pdf";
            fileResult.FileDownloadName = pdfFileName;

            return fileResult;
            
        }
    }
}
