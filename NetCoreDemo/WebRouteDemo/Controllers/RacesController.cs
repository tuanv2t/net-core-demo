﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebRouteDemo.Models;

namespace WebRouteDemo.Controllers
{

    /// <summary>
    /// Demo for issue: https://stackoverflow.com/questions/57772455/maproutes-in-net-core-2-2-mvc-c-sharp
    /// </summary>
    public class RacesController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Race(string raceName)
        {
            ViewBag.RaceName = raceName;
            return View();
        }


    }
}
