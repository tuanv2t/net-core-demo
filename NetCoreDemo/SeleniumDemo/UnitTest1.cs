using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
namespace SeleniumDemo
{
    [TestClass]
    public class UnitTest1
    {
        //Chrome driver: http://chromedriver.chromium.org/downloads
        //Selenium demo with chrome
        [TestMethod]
        public void TestMethod1()
        {
            //Download chrome driver in http://chromedriver.chromium.org/downloads and unzip in machine, such as C:\chromedriver_win32
            using (var driver = new ChromeDriver(@"C:\chromedriver_win32"))
            {
                //Navigate to DotNet website
                driver.Navigate().GoToUrl("https://dotnet.microsoft.com/");

            }
        }
    }
}
