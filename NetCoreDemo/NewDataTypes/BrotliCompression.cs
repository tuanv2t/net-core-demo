using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace NewDataTypes
{
    [TestClass]
    public class BrotliCompression
    {
        /// <summary>
        /// https://docs.microsoft.com/en-us/dotnet/core/whats-new/dotnet-core-2-1
        /// New Type BrotliCompression
        /// </summary>
        [TestMethod]
        public void BrotliCompressionTest()
        {
            int[] numbers = new int[100];
            for (int i = 0; i < 100; i++)
            {
                numbers[i] = i * 2;
            }
            var part = new Span<int>(numbers, start: 0, length: 10);
            foreach (var value in part)
                Trace.Write($"{value}  ");
        }
        /// <summary>
        ///Span<T> is a ref struct (stored on Stack) so that it's unable to do like this
        /// </summary>
        /// <returns></returns>
        //[TestMethod]
        //private async Task<Span<int>> GetAsyncSpanTest()
        //{
        //    int[] numbers = new int[100];
        //    for (int i = 0; i < 100; i++)
        //    {
        //        numbers[i] = i * 2;
        //    }
        //    var part = new Span<int>(numbers, start: 0, length: 10);
        //    return await Task.FromResult(part);
        //}
    }
}
