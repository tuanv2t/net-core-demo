using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace NewDataTypes
{
    /// <summary>
    /// Memory<T> is a new data type in net core 2.1 ( https://docs.microsoft.com/en-us/dotnet/core/whats-new/dotnet-core-2-1 )
    /// </summary>
    [TestClass]
    public class MemoryTest
    {
        /// <summary>
        /// https://docs.microsoft.com/en-us/dotnet/core/whats-new/dotnet-core-2-1
        /// New Type System.Span<T>
        /// </summary>
        [TestMethod]
        public void MemoryTest_OK()
        {
            int[] numbers = new int[100];
            for (int i = 0; i < 100; i++)
            {
                numbers[i] = i * 2;
            }
            var part = new Memory<int>(numbers, start: 0, length: 10).ToArray();
            foreach (var value in part)
                Trace.Write($"{value}  ");
        }
        /// <summary>
        /// Memory <T> is not ref structr ( stored on HEAP), so it is able to do this
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        private async Task<Memory<int>> GetAsyncMemory()
        {
            int[] numbers = new int[100];
            for (int i = 0; i < 100; i++)
            {
                numbers[i] = i * 2;
            }
            var part = new Memory<int>(numbers, start: 0, length: 10);
            return await Task.FromResult(part);
        }
        [TestMethod]
        public async Task GetAsyncMemory_OK()
        {
            var memory = await GetAsyncMemory();
            var part = memory.ToArray();
            foreach (var value in part)
                Trace.Write($"{value}  ");
        }
    }
}
