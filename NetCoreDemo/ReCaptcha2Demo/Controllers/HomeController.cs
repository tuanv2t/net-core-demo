﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ReCaptcha2Demo.Models;
using ReCaptcha2Demo.Securities;
namespace ReCaptcha2Demo.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        [HttpPost]
        [ServiceFilter(typeof(ValidateReCaptchaAttribute))]
        public IActionResult DemoRecaptcha()
        {
            if (ModelState.IsValid)
            {
                @ViewData["Message"] = "Recaptcha is valid";
            }
            else
            {
                @ViewData["Message"] = "Recaptcha is Invalid!!!";
            }
            return View();
        }
    }
}
