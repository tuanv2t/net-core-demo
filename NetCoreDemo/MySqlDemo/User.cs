﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MySqlDemo
{
    public class User
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
