using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MySqlDemo
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ConnectDatabase_OK()
        {
            using (var db = new PerformanceTestContext())
            {
            }
        }

        [TestMethod]
        public void CreateUser_OK()
        {
            using (var db = new PerformanceTestContext())
            {
                var item = new User
                {
                    Id = Guid.NewGuid().ToString(),
                    UserName = "AAAAA",
                    FirstName = "AAA",
                    LastName = "AAAAL"
                };
                db.Users.Add(item);
                db.SaveChanges();
            }
        }
        public string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }
        [TestMethod]
        public void CreateManyUser_OK()
        {
            //Generate one milion user 
            var numberOfItems = 1000000;
            var userList = new List<User>(numberOfItems);
            for (var i = 0; i < numberOfItems; i++)
            {
                userList.Add(new User()
                {
                    Id = Guid.NewGuid().ToString(),
                    UserName = RandomString(50, false),
                    FirstName = RandomString(50, false),
                    LastName = RandomString(50, false)
                });
            }
            int batchInsert = 0;
            using (var db = new PerformanceTestContext())
            {
                if (db.Users.Count() > 1000000)
                {
                    return;
                }
                foreach (var user in userList)
                {
                    batchInsert++;
                    db.Users.Add(user);
                    if (batchInsert == 10000)
                    {
                        db.SaveChanges();
                        batchInsert = 0;
                    }
                    

                }
                db.SaveChanges();//all remaining items
            }

            


        }


        [TestMethod]
        public void GetUser_OK()
        {
            //Note: you have to create index manually CREATE INDEX UserName_Idx on Users(UserName)
            using (var db = new PerformanceTestContext())
            {
                var start = DateTime.Now;
                var user = db.Users.FirstOrDefault(x => x.UserName.Equals("AAAAA"));
                var end = DateTime.Now;
                var duration = end.Subtract(start).TotalMilliseconds;
                //System.Diagnostics.Debug.WriteLine(duration);
                System.IO.File.CreateText(duration.ToString() + ".txt");
            }
        }
    }

   
}
